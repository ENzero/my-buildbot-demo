This is a demo buildbot project

# Set up
Follow installation instructions in https://docs.buildbot.net/latest/tutorial/docker.html

# Starting server
```script
cd ./simple
sudo docker-compose up
```
# To Do
- Generate Docker image based on buildbot worker image. Ongoing Dockerfile development in `pypy/wroker`. The docker image needs to be able to build a qt project, run cppcheck, and has dbus installed.
- This project needs to be migrated to bitbucket (ask Ed). As soon as it's done, update the tar link in `./simple/docker-compose.yml`
- Update `master.cfg` to point to bitbucket repo under test. Use line 10 instead of line 9. Some authentication needs to be updated as well.
- In `master.cfg`, under section `BUILDERS` (line 69), update the test steps to build, run test, and run static analysis.